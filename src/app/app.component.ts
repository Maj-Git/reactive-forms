import {
  AfterViewInit,
  Component,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
  ViewContainerRef, ViewRef
} from '@angular/core';
import {FormArray, FormBuilder, FormControl} from "@angular/forms";
import {MatSelect, MatSelectChange} from "@angular/material/select";

type AvailableFilter = 'creationDate' | 'creator';

type AvailableFilterTemplateRefs = Record<AvailableFilter, TemplateRef<any>>;

type FilterSelectValue = Record<AvailableFilter, string>;

type SearchFormGroup = {
  identifier: FormControl<string | null>,
  creator?: FormControl<string | null>,
  creationDate?: FormControl<Date | null>,
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements AfterViewInit {

  @ViewChild('addFilterSelectRef') addFilterSelectRef!: MatSelect;
  @ViewChild('additionalFiltersRef', {read: ViewContainerRef, static: true}) additionalFiltersRef!: ViewContainerRef;
  @ViewChild('creatorField') creatorField!: TemplateRef<any>;
  @ViewChild('creationDateField') creationDateField!: TemplateRef<any>;

  form = this.fb.group<SearchFormGroup>({
    identifier: this.fb.control<string>('')
  });

  availableFilters: FilterSelectValue = {
    creator: 'Creator',
    creationDate: 'Creation date'
  }

  private fields!: AvailableFilterTemplateRefs;
  private filtersEmbeddedViewRefs: Partial<Record<AvailableFilter, ViewRef>> = {}

  constructor(private readonly fb: FormBuilder) {
  }

  ngAfterViewInit(): void {
    this.fields = {
      creator: this.creatorField,
      creationDate: this.creationDateField,
    }
  }

  addFilter(filter: AvailableFilter) {

    if (!filter) {
      return;
    }

    this.form.addControl(filter, this.fb.control(null));
    this.filtersEmbeddedViewRefs[filter] = this.additionalFiltersRef.createEmbeddedView(this.fields[filter]);

    // clear selection
    this.addFilterSelectRef.options.forEach(x => x.deselect());

    console.log(this.form.value)
  }

  removeFilter(filter: AvailableFilter) {
    if (!filter) {
      return;
    }

    if (this.form.contains(filter)) {
      this.form.removeControl(filter);
    }

    const viewRef = this.filtersEmbeddedViewRefs[filter];
    if (viewRef) {
      const index = this.additionalFiltersRef.indexOf(viewRef);
      this.additionalFiltersRef.remove(index);
    }

  }

  submit() {
    console.log(this.form.value);
  }
}
